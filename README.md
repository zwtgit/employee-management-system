# springboot员工信息管理系统

#### 介绍
整合MyBatis和Shiro的员工管理系统。

#### 技术选型
| 技术 | 官网 |
|--- | ---|
|SpringBoot|https://docs.spring.io/spring-boot/docs/current/reference/html/ |
|MyBatis|https://mybatis.org/mybatis-3/zh/index.html |
|Shiro|http://shiro.apache.org/ |
|Redis|https://redis.io/|
|MySQL|https://www.mysql.com/|

#### 使用说明

1.  需要先创建对应的数据库或者自行修改数据库路径
2.  导入db文件夹中的db.sql
![](https://gitee.com/asstillwater/springboot-employee-management/raw/master/image/20210815121745.png)
3.  根据自己电脑的环境修改application.yml文件和applicaion-druid.yml文件
4.  浏览器访问：http://localhost 输入账号：admin 密码：123456
#### 成品效果如下图

![登录页面](https://gitee.com/asstillwater/springboot-employee-management/raw/master/image/20210813145915.png)
![登录后](https://gitee.com/asstillwater/springboot-employee-management/raw/master/image/20210813151302.png)
![登录后](https://gitee.com/asstillwater/springboot-employee-management/raw/master/image/20210813151322.png)
![登录后](https://gitee.com/asstillwater/springboot-employee-management/raw/master/image/20210813151344.png)

