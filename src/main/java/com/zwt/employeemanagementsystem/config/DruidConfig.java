package com.zwt.employeemanagementsystem.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource")
    @Bean
    public DataSource druidDataSource() {
        return new DruidDataSource();
    }

    //后台监控
    //这里都是死代码
    //因为springBoot内置了servlet容器，所以没有web.xmL，替代方法: ServletRegistrationBean
    @Bean
    public ServletRegistrationBean statViewServlet() {
        ServletRegistrationBean<StatViewServlet> statViewServletServletRegistrationBean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");

        //后台需要有人登陆的账号密码

        HashMap<String, String> initParameters = new HashMap<>();


        //增加配置，参数是固定的
        initParameters.put("loginUsername", "admin");
        initParameters.put("loginPassword", "123456");


        //允许谁可以访问
        initParameters.put("allow", "localhost");

        //禁止谁可以访问
//        initParameters.put("zwt","ip");

        //设置初始化参数
        statViewServletServletRegistrationBean.setInitParameters(initParameters);


        return statViewServletServletRegistrationBean;
    }


//    //配置 Druid 监控 之  web 监控的 filter
////WebStatFilter：用于配置Web和Druid数据源之间的管理关联监控统计
//    @Bean
//    public FilterRegistrationBean webStatFilter() {
//        FilterRegistrationBean bean = new FilterRegistrationBean();
//        bean.setFilter(new WebStatFilter());
//
//        //exclusions：设置哪些请求进行过滤排除掉，从而不进行统计
//        Map<String, String> initParams = new HashMap<>();
//        initParams.put("exclusions", "*.js,*.css,/druid/*,/jdbc/*");
//        bean.setInitParameters(initParams);
//
//        //"/*" 表示过滤所有请求
//        bean.setUrlPatterns(Arrays.asList("/*"));
//        return bean;
//    }


}
