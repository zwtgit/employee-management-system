package com.zwt.mybatis.mapper;

import com.zwt.mybatis.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

//注解表示这是mybatis的mapper
//@Repository用在持久层的接口上，这个注解是将接口的一个实现类交给spring管理。
@Mapper
@Repository
public interface UserMapper {

    List<User> queryUserList();

    User queryUserById(int id);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int id);

}
